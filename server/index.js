var express = require("express");
var fs = require("fs");
var app = express();
const PORT = process.env.PORT || 3003;

app.use((req, res, next) => {
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.get("/api/data", (req, res) => {
    fs.readFile('./data/templates.json',"utf8", (err, data)=> {
        if(err) res.status(500).send("Something went wrong");
        res.status(200).send(data)
    })
});

app.get("/api/largeData", (req, res) => {
    let templates = '';
    fs.readFile('./data/templates.json', (err, data)=> {
        if(err) throw err;
        templates = JSON.parse(data);
    })
    res.status(200).send('{test}')
});

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});